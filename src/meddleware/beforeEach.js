import {router} from '@/router'
import {session} from '@/libs/session'

router.beforeEach((to, from, next) => {
    if (!to.matched.length && to.path !== '/404') {
        next({path: '/404', query: {redirect: to.fullPath}})
        return
    }
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (!session.get()) {
            next({
                path: '/login',
                query: {redirect: to.fullPath}
            })
        } else {
            next()
        }
    } else {
        next()
    }
})
