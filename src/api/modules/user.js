import {http} from '@/libs/http'

export const user = {
    profile: (data = {}) => {
        return http.request({
            url: '/api/user/profile',
            data
        })
    }
}
