import {http} from '@/libs/http'

export const permission = {
    routes: (data = {}) => {
        return http.request({
            url: '/api/permission/routes',
            data
        })
    }
}
