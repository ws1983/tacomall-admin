import {user} from './modules/user'
import {permission} from './modules/permission'

export default {
    user,
    permission
}
