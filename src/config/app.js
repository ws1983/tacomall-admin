export const appConfig  = {
    baseUrl: {
        dev: 'http://localhost:1400/',
        prod: 'http://api.neho.top/'
    },
    tokenKey: 'x-access-token'
}
