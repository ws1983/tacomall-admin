import * as types from './mutation-types'
import api from '@/api'
import {session} from '@/libs/session'

export const actions = {
    async profile({commit}) {
        const resp = await api.user.profile()
        if (resp.done) {
            commit(types.SET_PROFILE, resp.data)
            commit('setIsLogin', true, {root: true})
        }
    },
    async exit({commit}) {
        return new Promise((resolve) => {
            session.exit()
            commit(types.SET_PROFILE, {})
            commit('setIsLogin', false, {root: true})
            resolve()
        })
    }
}
