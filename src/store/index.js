import Vue from 'vue'
import Vuex from 'vuex'

import user from './user'
import permission from './permission'

import {session} from '@/libs/session'

Vue.use(Vuex)

export const store = new Vuex.Store({
    modules: {
        user,
        permission
    },
    state: {
        isLogin: false
    },
    mutations: {
        setIsLogin(state, b) {
            state.isLogin = b
        }
    },
    actions: {
        appInit({dispatch}) {
            session.get() && dispatch('user/profile')
            session.get() && dispatch('permission/routes')
        }
    }
})
