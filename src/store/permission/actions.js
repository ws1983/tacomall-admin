import api from '@/api'
import {menuUtil} from '@/utils/menuUtil'
import {dynamicRoutes} from '@/router/routes/dynamic'
import * as types from './mutation-types'
import {router} from '@/router'

export const actions = {
    async routes({commit}) {
        const resp = await api.permission.routes()
        if (resp.done) {
            let permissionRoutes = []
            let respK = resp.data.map(i => {
                return i.path
            })
            let dynamicK = dynamicRoutes.map(j => {
                return j.path
            })
            for (let i = 0; i < respK.length; i++) {
                let index = dynamicK.indexOf(respK[i])
                if (index >= 0) {
                    permissionRoutes.push(dynamicRoutes[index])
                }
            }
            router.addRoutes(permissionRoutes)
            commit(types.SET_MENU, menuUtil.getMenuByRoutes(permissionRoutes))
        }
    }
}
