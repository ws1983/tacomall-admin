export const menuUtil = {
    getMenuByRoutes: (routes) => {
        let menu = []
        for (let i = 0; i < routes.length; i++) {
            if (routes[i].meta && routes[i].meta.isMenu) {
                let menuK = menu.map(i => {
                    return i.pid
                })
                if (!menuK.includes(routes[i].meta.pid)) {
                    menu.push({
                        pid: routes[i].meta.pid,
                        name: routes[i].meta.pName,
                        list: [routes[i]]
                    })
                    continue
                }
                menu[menuK.indexOf(routes[i].meta.pid)].list.push(routes[i])
            }
        }
        return menu
    }
}
