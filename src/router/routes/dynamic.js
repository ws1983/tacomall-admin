export const dynamicRoutes = [
    {
        name: "/goods-list",
        path: "/goods-list",
        meta: {
            pid: 'goods',
            pName: '商品',
            isMenu: true,
            title: '商品列表',
            requiresAuth: true
        },
        component: () => import('@/pages/goods-list/index')
    },
    {
        name: "/order-list",
        path: "/order-list",
        meta: {
            pid: 'order',
            pName: '订单',
            isMenu: true,
            title: '订单列表',
            requiresAuth: true
        },
        component: () => import('@/pages/order-list/index')
    },
]
