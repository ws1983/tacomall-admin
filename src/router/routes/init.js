export const initRoutes = [
    {
        name: "/login",
        path: "/login",
        meta: {
            isMenu: false,
            title: '登录',
            requiresAuth: false
        },
        component: () => import('@/pages/login/index')
    },
    {
        name: "/index",
        path: "/",
        meta: {
            isMenu: false,
            title: '首页',
            requiresAuth: true
        },
        component: () => import('@/pages/index/index')
    },
    {
        name: "/404",
        path: "/404",
        meta: {
            isMenu: false,
            title: '404',
            requiresAuth: false
        },
        component: () => import('@/pages/404/index')
    }
]
